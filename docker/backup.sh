#!/usr/bin/env bash

# Use `borg` to backup the directory at `${VOLUME_MOUNT_PATH}`

# This is a modified template from the official borg quickstart manual:
#   https://github.com/borgbackup/borg/blob/master/docs/quickstart.rst

set -o errexit  # exit on first error
set -o nounset  # exit if undefined variable referenced
set -o pipefail  # set pipe exit code to failure if any command in pipe fails


# Some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap "echo $( date ) Backup interrupted >&2; exit 2" INT TERM


# Use first argument as the backup name prefix if one is provided
if [[ ! -z "${1+x}" ]]; then
    BACKUP_NAME="${1}__{now}"
else
    BACKUP_NAME="{now}"
fi


# Set repo passphrase
if [[ -z "${BORG_PASSPHRASE+x}" ]]; then
    info "Environment variable BORG_PASSPHRASE unset, reading repo password from file..."

    if [[ ! -f "${REPO_PASSWORD_CLEARTEXT_PATH}" ]]; then
        info "ERROR: Password file ${REPO_PASSWORD_CLEARTEXT_PATH} does not exist!"
        exit 1
    fi
    export BORG_PASSPHRASE=$(cat ${REPO_PASSWORD_CLEARTEXT_PATH})

    if [[ -z "${BORG_PASSPHRASE}" ]]; then
        info "ERROR: Repo password must NOT be empty!"
        exit 1
    fi
fi


# The SSH config files need to be owned by the current user. We're working
# around this by copying the original read-only files into the current user's
# SSH config directory.
info "Preparing SSH configs..."
cp -R ${SSH_CONFIG_DIR} ~/.ssh/

# Initialize the repo if it doesn't yet exist
info "Checking if repository exists..."
if ! borg info; then
    info "Repository does not exist, creating a new repo"
    borg init \
        --encryption repokey
fi


# Backing up relative paths by moving into the correct directory first
cd ${BACKUP_SOURCE_DIR}

info "Starting backup with name '${BACKUP_NAME}' ..."

# Do backup
borg create                         \
    --progress                      \
    --filter AME                    \
    --list                          \
    --stats                         \
    --show-rc                       \
    --compression lz4               \
    --exclude-caches                \
                                    \
    ::"${BACKUP_NAME}"              \
    .


backup_exit=$?

info "Pruning repository..."

# Use the prune subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine.

borg prune                          \
    --list                          \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [[ ${global_exit} -eq 0 ]]; then
    info "Backup and Prune finished successfully"
elif [[ ${global_exit} -eq 1 ]]; then
    info "Backup and/or Prune finished with warnings"
else
    info "Backup and/or Prune finished with errors"
fi

exit ${global_exit}
